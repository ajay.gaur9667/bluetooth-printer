package com.example.bluetoothhindiprint

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions


object RuntimePermissions {

    @RequiresApi(Build.VERSION_CODES.S)
    private const val bluetoothScanPermission = Manifest.permission.BLUETOOTH_SCAN
    @RequiresApi(Build.VERSION_CODES.S)
    private const val bluetoothConnectPermission = android.Manifest.permission.BLUETOOTH_CONNECT
    // private const val smsPermissions = android.Manifest.permission.SEND_SMS

    fun checkVersion(): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.S
    }

    fun checkBlueToothPermission(context: Context): Boolean {
        val blueToothConnect =
            ContextCompat.checkSelfPermission(context, Manifest.permission.BLUETOOTH_CONNECT)
        val blueToothScan =
            ContextCompat.checkSelfPermission(context, Manifest.permission.BLUETOOTH_SCAN)
        return blueToothConnect == PackageManager.PERMISSION_GRANTED || blueToothScan == PackageManager.PERMISSION_GRANTED
    }


    fun getBlueToothPermission(context: Context) {
        val btPermissionArray = arrayOf(bluetoothConnectPermission, bluetoothScanPermission)
        Permissions.check(
            context,
            btPermissionArray,
            null,
            null,
            object : PermissionHandler() {
                override fun onGranted() {
                    // do your task.
                }
            })
    }


}