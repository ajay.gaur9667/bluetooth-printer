package com.example.bluetoothhindiprint;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class StringToImageConverter {

    public static Bitmap convertToBitmap(String text, int width, int height) {
        // Create a bitmap with white background
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(Color.WHITE);

        // Draw text on the bitmap
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.BLACK);
        paint.setTextSize(30f); // Set text size as needed
        paint.setTextAlign(Paint.Align.LEFT); // Set text alignment
        float x = 20f; // Set x coordinate
        float y = 30f; // Set y coordinate
        canvas.drawText(text, x, y, paint);

        return bitmap;
    }
}

