package com.example.bluetoothhindiprint

import android.graphics.Bitmap

object PrinterCommand {

    // Initialize the printer
    fun init(): ByteArray {
        return byteArrayOf(0x1B, 0x40)
    }

    // Align text to the center
    fun alignCenter(): ByteArray {
        return byteArrayOf(0x1B, 0x61, 0x01)
    }

    // Print a bitmap
    fun printBitmap(bitmap: Bitmap): ByteArray {
        // Convert bitmap to a byte array for printing
        // This implementation is simplified and might not work for all printers
        val bytes = ByteArray(bitmap.width * bitmap.height)
        for (x in 0 until bitmap.width) {
            for (y in 0 until bitmap.height) {
                val pixel = bitmap.getPixel(x, y)
                // Convert color to grayscale and set as intensity
                val intensity = (0.299 * android.graphics.Color.red(pixel) +
                        0.587 * android.graphics.Color.green(pixel) +
                        0.114 * android.graphics.Color.blue(pixel)).toInt()
                bytes[y * bitmap.width + x] = if (intensity < 128) 0 else 1
            }
        }
        return bytes
    }

    // Feed line
    fun feedLine(lines: Int): ByteArray {
        return byteArrayOf(0x1B, 0x64, lines.toByte())
    }
}