package com.example.bluetoothhindiprint

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.BluetoothSocket
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import android.graphics.pdf.PdfRenderer
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.os.ParcelFileDescriptor
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.util.UUID

@SuppressLint("HandlerLeak")
open class MainActivity : AppCompatActivity(), Runnable {
    private lateinit var titlePairedDevices: TextView
    private lateinit var mPrint: Button
    private lateinit var mDisc: Button
    private lateinit var printValueTextView: TextView
    private var mBluetoothAdapter: BluetoothAdapter? = null
    private val applicationUUID = UUID
        .fromString("00001101-0000-1000-8000-00805F9B34FB")
    private var mBluetoothConnectProgressDialog: ProgressDialog? = null
    private var mBluetoothSocket: BluetoothSocket? = null
    private lateinit var mBluetoothDevice: BluetoothDevice
    private var mPairedDevicesArrayAdapter: ArrayAdapter<String>? = null
    private var mDeviceAddress: String = ""

    private val mHandler = object : Handler() {
        override fun handleMessage(msg: Message) {
            mBluetoothConnectProgressDialog!!.dismiss()
            Toast.makeText(this@MainActivity, "DeviceConnected", Toast.LENGTH_SHORT).show()
        }
    }

    @RequiresApi(Build.VERSION_CODES.S)
    @SuppressLint("MissingPermission")
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mPrint = findViewById(R.id.mPrint)
        printValueTextView = findViewById(R.id.printValueTextView)
        mPrint.setOnClickListener {
            val t = object : Thread() {
                override fun run() {
                    try {
                        if (mBluetoothSocket != null) {
                            val os = mBluetoothSocket!!
                                .outputStream


                            val hindiText = "अजय शर्मा कुणाल पुनिया कुणाल पुनिया कुणाल पुनिया"

                            runOnUiThread {

                                // Convert the string to a PDF
                                val pdfFile = createPdfFromText(hindiText)

                                if (pdfFile != null) {
                                    // Convert the PDF to a Bitmap
                                    val bitmap = pdfToBitmap(pdfFile,os)

                                    if (bitmap != null) {
                                        try {
                                            //printPhoto(bitmap, os)
                                        } catch (e: IOException) {
                                            e.printStackTrace()
                                        }
                                        Log.e("MainActivity", "Exe ")
                                        // Use the bitmap as needed (e.g., display in ImageView)
                                    } else {
                                        Log.e("MainActivity", "Exe ")
                                    }
                                } else {
                                    Log.e("MainActivity", "Exe ")
                                }

                               /* val bitmap = textToBitmap(hindiText, applicationContext)
                                sendBitmapToPrinter(bitmap,os)*/
                                // Update UI elements here
                                /*val result = splitStringIntoLines(hindiText, 29)

                                for (line in result) {
                                    println(line)

                                    printValueTextView.text = line
                                    val width = printValueTextView.width //400 // Set the width of the image
                                    val height =
                                        printValueTextView.height//extractHeight(text)  //200 // Set the height of the image
                                    val bitmap = StringToImageConverter.convertToBitmap(line, width, height)
                                    Log.e("Main","$bitmap")
                                     printPhoto(bitmap, os)
                                }*/
                            }
                        } else {
                            Log.e("MainActivity", "Exe please connect")
                        }
                    } catch (e: Exception) {
                        Log.e("MainActivity", "Exe ", e)
                    }

                }
            }
            t.start()
        }

        mDisc = findViewById(R.id.dis)
        mDisc.setOnClickListener {
            if (mBluetoothAdapter != null)
                mBluetoothAdapter!!.disable()
        }
        scanBluetoothDevice()

    }// onCreate


    override fun onDestroy() {
        super.onDestroy()
        try {
            if (mBluetoothSocket != null)
                mBluetoothSocket!!.close()
        } catch (e: Exception) {
            Log.e("Tag", "Exe ", e)
        }

    }

    @SuppressLint("MissingPermission")
    override fun run() {
        try {
            mBluetoothSocket = mBluetoothDevice
                .createRfcommSocketToServiceRecord(applicationUUID)
            mBluetoothAdapter!!.cancelDiscovery()
            mBluetoothSocket!!.connect()
            mHandler.sendEmptyMessage(0)
        } catch (eConnectException: IOException) {
            Log.d(TAG, "CouldNotConnectToSocket", eConnectException)
            closeSocket(this.mBluetoothSocket!!)
            return
        }

    }

    private fun closeSocket(nOpenSocket: BluetoothSocket) {
        try {
            nOpenSocket.close()
            Log.d(TAG, "SocketClosed")
        } catch (ex: IOException) {
            Log.d(TAG, "CouldNotCloseSocket")
        }

    }


    companion object {
        protected const val TAG = "TAG"
    }


    @SuppressLint("MissingPermission")
    @RequiresApi(Build.VERSION_CODES.S)
    fun scanBluetoothDevice() {
        mPairedDevicesArrayAdapter = ArrayAdapter(this, R.layout.device_name)
        val mPairedListView = findViewById<ListView>(R.id.paired_devices)
        mPairedListView.adapter = mPairedDevicesArrayAdapter
        if (RuntimePermissions.checkVersion()) {
            if (!RuntimePermissions.checkBlueToothPermission(this)) {
                RuntimePermissions.getBlueToothPermission(this)
            } else {
                mPairedListView.onItemClickListener = mDeviceClickListener
            }
        } else {
            mPairedListView.onItemClickListener = mDeviceClickListener
        }

        val bluetoothManager = this.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        mBluetoothAdapter = bluetoothManager.adapter
        val mPairedDevices = mBluetoothAdapter!!.bondedDevices

        if (mPairedDevices.size > 0) {
            titlePairedDevices = findViewById(R.id.title_paired_devices)
            titlePairedDevices.visibility = View.VISIBLE
            for (mDevice in mPairedDevices) {
                mPairedDevicesArrayAdapter!!.add(mDevice.name + "\n" + mDevice.address)
            }
        } else {
            val mNoDevices = "None Paired"
            mPairedDevicesArrayAdapter!!.add(mNoDevices)
        }
    }

    @SuppressLint("MissingPermission")
    @RequiresApi(Build.VERSION_CODES.S)
    private val mDeviceClickListener = AdapterView.OnItemClickListener { _, mView, _, _ ->
        if (RuntimePermissions.checkVersion()) {
            if (!RuntimePermissions.checkBlueToothPermission(this)) {
                RuntimePermissions.getBlueToothPermission(this)
            } else {
                try {
                    mBluetoothAdapter!!.cancelDiscovery()
                    val mDeviceInfo = (mView as TextView).text.toString()
                    mDeviceAddress = mDeviceInfo.substring(mDeviceInfo.length - 17)
                    Log.d("MainActivity", "Device_Address $mDeviceAddress")
                    mBluetoothDevice = mBluetoothAdapter!!
                        .getRemoteDevice(mDeviceAddress)
                    mBluetoothConnectProgressDialog = ProgressDialog.show(
                        this,
                        "Connecting...", mBluetoothDevice.name + " : "
                                + mBluetoothDevice.address, true, false
                    )
                    val mBluetoothConnectThread = Thread(this)
                    mBluetoothConnectThread.start()
                } catch (ex: Exception) {
                    Log.d("MainActivity", "${ex.message}")
                }
            }
        } else {
            try {
                mBluetoothAdapter!!.cancelDiscovery()
                val mDeviceInfo = (mView as TextView).text.toString()
                mDeviceAddress = mDeviceInfo.substring(mDeviceInfo.length - 17)
                Log.d("MainActivity", "Device_Address $mDeviceAddress")
                mBluetoothDevice = mBluetoothAdapter!!
                    .getRemoteDevice(mDeviceAddress)
                mBluetoothConnectProgressDialog = ProgressDialog.show(
                    this,
                    "Connecting...", mBluetoothDevice.name + " : "
                            + mBluetoothDevice.address, true, false
                )
                val mBluetoothConnectThread = Thread(this)
                mBluetoothConnectThread.start()
            } catch (ex: Exception) {
                Log.d("MainActivity", "Device_Address $ex")
            }
        }
    }

    fun printPhoto(bmp: Bitmap, outputStream: OutputStream) {
        try {
            val command: ByteArray = Utils.decodeBitmap(bmp)
            outputStream.write(PrinterCommands.ESC_ALIGN_LEFT)
            printText(command, outputStream)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            Log.e("PrintTools", "the file isn't exists")
        }
    }

    private fun printText(msg: ByteArray, outputStream: OutputStream) {
        try {
            // Print normal text
            outputStream.write(msg)
            printNewLine(outputStream)
            printNewLine(outputStream)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun printNewLine(outputStream: OutputStream) {
        try {
            outputStream.write(PrinterCommands.FEED_LINE)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }



    fun splitStringIntoLines(inputString: String, maxLineLength: Int): List<String> {
        val lines = mutableListOf<String>()
        var currentLine = ""

        for (word in inputString.split(" ")) {
            if ((currentLine.length + word.length) <= maxLineLength) {
                // If adding the word to the current line does not exceed the max length, add it
                if (currentLine.isNotEmpty()) {
                    currentLine += " "
                }
                currentLine += word
            } else {
                // If adding the word would exceed the max length, start a new line
                lines.add(currentLine)
                currentLine = word
            }
        }

        // Add the last line
        if (currentLine.isNotEmpty()) {
            lines.add(currentLine)
        }

        return lines
    }

    private fun textToBitmap(text: String, context: Context): Bitmap {
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.textSize = 30f
        paint.color = context.resources.getColor(android.R.color.black)
        val baseline = -paint.ascent()
        val width = (paint.measureText(text) + 0.5f).toInt()
        val height = (baseline + paint.descent() + 0.5f).toInt()
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        canvas.drawText(text, 0f, baseline, paint)
        return bitmap
    }

    private fun sendBitmapToPrinter(bitmap: Bitmap,outputStream:OutputStream) {
        val handler = Handler(Looper.getMainLooper())
        Thread {
            try {
                outputStream.write(PrinterCommand.init())
                outputStream.write(PrinterCommand.alignCenter())
                outputStream.write(PrinterCommand.printBitmap(bitmap))
                outputStream.write(PrinterCommand.feedLine(3))
            } catch (e: IOException) {
                handler.post {
                    Toast.makeText(applicationContext, "Error printing: ${e.message}", Toast.LENGTH_SHORT).show()
                }
            }
        }.start()
    }

    private fun createPdfFromText(text: String): File? {
        try {
            val file = File.createTempFile("temp_pdf_file", ".pdf")
            val pdfDocument = PdfDocument()
            val pageInfo = PdfDocument.PageInfo.Builder(300, 600, 1).create()
            val page = pdfDocument.startPage(pageInfo)
            val canvas = page.canvas
            val paint = Paint()
            paint.color = android.graphics.Color.BLACK
            paint.textSize = 40f
            val textLines = text.split("\n")
            var startY = 40f
            for (line in textLines) {
                canvas.drawText(line, 40f, startY, paint)
                startY += paint.descent() - paint.ascent()
            }
            pdfDocument.finishPage(page)
            val outputStream = FileOutputStream(file)
            pdfDocument.writeTo(outputStream)
            pdfDocument.close()
            outputStream.close()
            return file
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }

    private fun pdfToBitmap(pdfFile: File,outputStream:OutputStream): Bitmap? {
        val renderer = PdfRenderer(ParcelFileDescriptor.open(pdfFile, ParcelFileDescriptor.MODE_READ_ONLY))
        val page = renderer.openPage(0)
        val bitmap = Bitmap.createBitmap(page.width, page.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        canvas.drawColor(android.graphics.Color.WHITE)
        page.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY)
        page.close()
        renderer.close()
        //bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
        return bitmap
    }


}